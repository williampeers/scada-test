class CreatePierrors < ActiveRecord::Migration[5.1]
  def change
    create_table :pierrors do |t|
      t.text :details
      t.references :variable, foreign_key: true
      t.references :unit, foreign_key: true

      t.timestamps
    end
  end
end
