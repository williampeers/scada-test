class AddColumnsToUnits < ActiveRecord::Migration[5.1]
  def change
    add_column :units, :psk, :string
    add_column :units, :identification, :integer
  end
end
