class UsersController < ApplicationController
  layout 'application'
  
  def show
    @user = current_user
  end
  
  def new
    @user = User.new
  end
  
  def create
    if (user_params.has_key?(:admin) and user_params[:admin] == true and not admin?)
      flash[:danger] = "Only admin accounts can create admin accounts"
      redirect_to root_url
    end
    @user = User.new(user_params)
    if @user.save
      log_in @user if not admin?
      flash[:success] = "Acount successfully created"
      redirect_to current_user
    else
      render 'new'
    end
  end

  def createAdmin
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Acount successfully created"
      redirect_to current_user
    else
      render 'new'
    end
  end
  
  def index
    if admin?
      @users = User.all
      @user = User.new
    else
      flash[:danger] = "Must be admin"
      redirect_to root_url
    end
  end
  
  def account
  end

  private

    def user_params
      params[:user][:name].gsub!(' ', '_')
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :admin)
    end

    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    # Confirms the correct user.
    def correct_user
      unless allowed_user?
        redirect_to login_url
      end
    end
end
