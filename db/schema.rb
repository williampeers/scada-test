# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171127013822) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "datapoints", force: :cascade do |t|
    t.datetime "time"
    t.float "value"
    t.integer "variable_id"
    t.index ["time", "variable_id"], name: "index_datapoints_on_time_and_variable_id"
  end

  create_table "inputs", force: :cascade do |t|
    t.integer "register_address"
    t.integer "bit_index"
    t.integer "length"
    t.string "units"
    t.decimal "current_value"
    t.datetime "last_update"
    t.decimal "new_value"
    t.boolean "update_value"
    t.bigint "unit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["unit_id"], name: "index_inputs_on_unit_id"
  end

  create_table "outputs", force: :cascade do |t|
    t.integer "register_address"
    t.integer "bit_index"
    t.integer "length"
    t.string "units"
    t.decimal "current_value"
    t.datetime "last_update"
    t.bigint "unit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.float "last_month", default: [], array: true
    t.float "last_year", default: [], array: true
    t.float "alltime", default: [], array: true
    t.index ["unit_id"], name: "index_outputs_on_unit_id"
  end

  create_table "pierrors", force: :cascade do |t|
    t.text "details"
    t.bigint "variable_id"
    t.bigint "unit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "time"
    t.index ["unit_id"], name: "index_pierrors_on_unit_id"
    t.index ["variable_id"], name: "index_pierrors_on_variable_id"
  end

  create_table "units", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "plc_ip"
    t.integer "plc_port"
    t.string "ip"
    t.bigint "user_id"
    t.boolean "new_config"
    t.integer "short_update_period", default: 1
    t.boolean "working", default: true
    t.string "psk"
    t.integer "identification"
    t.integer "long_update_period", default: 300
    t.index ["identification"], name: "index_units_on_identification"
    t.index ["user_id"], name: "index_units_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.boolean "admin"
  end

  create_table "variables", force: :cascade do |t|
    t.string "name"
    t.bigint "unit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "register_address"
    t.integer "bit_index"
    t.integer "length"
    t.string "units"
    t.string "type"
    t.string "psk"
    t.decimal "current_value", default: "0.0"
    t.datetime "last_update"
    t.string "method"
    t.boolean "update_value"
    t.decimal "new_value"
    t.boolean "working", default: true
    t.string "dataName"
    t.index ["unit_id", "created_at"], name: "index_variables_on_unit_id_and_created_at"
    t.index ["unit_id"], name: "index_variables_on_unit_id"
  end

  add_foreign_key "inputs", "units"
  add_foreign_key "outputs", "units"
  add_foreign_key "pierrors", "units", on_delete: :cascade
  add_foreign_key "pierrors", "variables", on_delete: :cascade
end
