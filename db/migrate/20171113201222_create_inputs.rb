class CreateInputs < ActiveRecord::Migration[5.1]
  def change
    create_table :inputs do |t|
      t.integer :register_address
      t.integer :bit_index
      t.integer :length
      t.string :units
      t.decimal :current_value
      t.datetime :last_update
      t.decimal :new_value
      t.boolean :update_value
      t.references :unit, foreign_key: true

      t.timestamps
    end
  end
end
