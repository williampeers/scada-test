class DropDatapoints < ActiveRecord::Migration[5.1]
  def change
    drop_table :datapoints
  end
end
