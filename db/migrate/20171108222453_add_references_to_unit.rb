class AddReferencesToUnit < ActiveRecord::Migration[5.1]
  def change
    add_reference :units, :user, index: true
  end
end
