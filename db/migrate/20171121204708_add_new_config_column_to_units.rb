class AddNewConfigColumnToUnits < ActiveRecord::Migration[5.1]
  def change
    add_column :units, :new_config, :boolean
    add_column :units, :update_period, :integer, default: 2
  end
end
