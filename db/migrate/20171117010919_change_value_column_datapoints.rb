class ChangeValueColumnDatapoints < ActiveRecord::Migration[5.1]
  def change
    remove_column :datapoints, :value
    add_column :datapoints, :value, :decimal
  end
end
