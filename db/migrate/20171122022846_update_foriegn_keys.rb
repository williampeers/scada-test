class UpdateForiegnKeys < ActiveRecord::Migration[5.1]
  def change
    remove_foreign_key :pierrors, :variables
    add_foreign_key :pierrors, :variables, on_delete: :cascade
    remove_foreign_key :pierrors, :units
    add_foreign_key :pierrors, :units, on_delete: :cascade
  end
end
