module SessionsHelper
  def log_in(user)
    session[:user_id] = user.id
  end
  
  def current_user
    user = User.find_by(id: session[:user_id])
    if user and user.admin == true
      if params.key?(:user_name)
        @current_user = User.find_by(name: params[:user_name])
      else
        user
      end
    else
      @current_user ||= User.find_by(id: session[:user_id])
    end
  end
  
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end
  
  def logged_in?
    current_user or User.find_by(id: session[:user_id])
  end
  
  def admin?
    logged_in? and User.find_by(id: session[:user_id]).admin == true
  end
  
  def allowed_user?
    return true if admin?
    if params.has_key?(:user_name)
      return false if not logged_in?
      current_user.name == params[:user_name]
    else
      true
    end
  end
end
