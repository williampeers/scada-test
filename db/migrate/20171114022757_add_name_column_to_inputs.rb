class AddNameColumnToInputs < ActiveRecord::Migration[5.1]
  def change
    add_column :inputs, :name, :string
    add_column :outputs, :name, :string
  end
end
