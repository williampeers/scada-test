class CreateTimeseries < ActiveRecord::Migration[5.1]
  def change
    create_table :timeseries do |t|
      t.datetime :time
      t.decimal :value
      t.references :variable, foreign_key: true

      t.timestamps
    end
  end
end
