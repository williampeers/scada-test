class AddColumnsToOutputs < ActiveRecord::Migration[5.1]
  def change
    add_column :outputs, :last_month, :float, array: true, default: []
    add_column :outputs, :last_year, :float, array: true, default: []
    add_column :outputs, :alltime, :float, array: true, default: []
  end
end
