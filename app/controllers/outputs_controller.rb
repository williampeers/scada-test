class OutputsController < ApplicationController
 
  before_action :correct_user
  
  def create
    @unit = current_user.units.find_by(name: params[:unit_name])
    @output = @unit.outputs.build(output_params)
    if @output.save
      redirect_to @unit
    else
      redirect_to @unit
    end
  end
  
  def edit
    @unit  = current_user.units.find_by(name: params[:unit_name])
    @output = @unit.outputs.find_by(name: params[:output_name])
  end
  
  def patch
    @unit  = current_user.units.find_by(name: params[:unit_name])
    @output = @unit.outputs.find_by(name: params[:output_name])
    @output.update(output_params)
    redirect_to @unit
  end
  
  def show
    @unit  = current_user.units.find_by(name: params[:unit_name])
    @output = @unit.outputs.find_by(name: params[:output_name])
  end
  
  def new
    @unit  = current_user.units.find_by(name: params[:unit_name])
    @output = @unit.outputs.new
  end
  
  private
    def output_params
      params.require(:output).permit(:name, :register_address, :bit_index, :length, :units, :unit_id)
    end
    
    def correct_user
      unless allowed_user?
        redirect_to login_url
      end
    end
end
class OutputsController < ApplicationController
 
  before_action :correct_user
  
  def create
    @unit = current_user.units.find_by(name: params[:unit_name])
    @output = @unit.outputs.build(output_params)
    if @output.save
      redirect_to @unit
    else
      redirect_to @unit
    end
  end
  
  def show
    @unit  = current_user.units.find_by(name: params[:unit_name])
    @output = @unit.outputs.find_by(name: params[:output_name])
  end
  
  def new
    @unit  = current_user.units.find_by(name: params[:unit_name])
    @output = @unit.outputs.new
  end
  
  private
    def output_params
      params.require(:output).permit(:name, :register_address, :bit_index, :length, :units, :unit_id)
    end
    
    def correct_user
      unless allowed_user?
        redirect_to login_url
      end
    end
end
