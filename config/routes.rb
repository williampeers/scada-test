Rails.application.routes.draw do

  root 'application#home'

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  get '/signup', to: 'users#new'
  get '/users', to: 'users#index', as: 'show_users'
  
  get '/:user_name', to: 'users#show', as: 'user'
  get '/:user_name/account', to: 'users#account', as: 'account'
  post '/:user_name/new-unit', to: 'units#create', as: 'create_unit'
  get '/:user_name/new-unit', to: 'units#new', as: 'new_unit'
  get '/:user_name/:unit_name/', to: 'units#show', as: 'unit'
  
  get '/:user_name/:unit_name/edit', to: 'units#edit', as: 'edit_unit'
  patch '/:user_name/:unit_name/edit', to: 'units#patch'
  
  get '/:user_name/:unit_name/delete', to: 'units#delete', as: 'delete_unit'
  post '/:user_name/:unit_name/delete', to: 'units#destroy'
  
  post '/:user_name/:unit_name/update', to: 'units#update'
  
  get '/:user_name/:unit_name/new-variable', to: 'variables#new', as: 'new_variable'
  post '/:user_name/:unit_name/new-variable', to: 'variables#create'
  
  get '/:user_name/:unit_name/:variable_name', to: 'variables#show', as: 'variable'
  get '/:user_name/:unit_name/:variable_name/chart', to: 'variables#chart', as: 'chart'
  get '/:user_name/:unit_name/:variable_name/edit', to: 'variables#edit', as: 'edit_variable'
  patch '/:user_name/:unit_name/:variable_name/edit', to: 'variables#patch'
  get '/:user_name/:unit_name/:variable_name/delete', to: 'variables#delete', as: 'delete_variable'
  post '/:user_name/:unit_name/:variable_name/delete', to: 'variables#destroy'
  
  patch '/:user_name/:unit_name/:variable_name/update', to: 'variables#update', as: 'update_variable'
  
  #For raspberry pi communication
  post '/units/:identification', to: 'units#pi_update'


  resources :datapoints, only: [:create]
  resources :users
  resources :units, as: :unit do#, :path => '/:unit-id' do
    resources :variables, :outputs
  end
end

=begin
     pages GET    /pages(.:format)          pages#index
           POST   /pages(.:format)          pages#create
  new_page GET    /pages/new(.:format)      pages#new
 edit_page GET    /pages/:id/edit(.:format) pages#edit
      page GET    /pages/:id(.:format)      pages#show
           PATCH  /pages/:id(.:format)      pages#update
           PUT    /pages/:id(.:format)      pages#update
           DELETE /pages/:id(.:format)      pages#destroy
=end           