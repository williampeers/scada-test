class RemakeDatapoints < ActiveRecord::Migration[5.1]
  def change
    drop_table :datapoints
    create_table :datapoints do |t|
      t.datetime :time
      t.float :value
      t.integer :variable_id
    end
    add_index :datapoints, [:time, :variable_id]
  end
end
