class AddDetailsToVariables < ActiveRecord::Migration[5.1]
  def change
    add_column :variables, :register_address, :integer
    add_column :variables, :bit_index, :integer
    add_column :variables, :length, :integer
    add_column :variables, :units, :string
    add_column :variables, :type, :string
    add_column :variables, :psk, :string
    
    add_column :units, :plc_ip, :string
    add_column :units, :plc_port, :integer
    add_column :units, :ip, :string
    
  end
end
