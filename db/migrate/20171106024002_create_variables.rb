class CreateVariables < ActiveRecord::Migration[5.1]
  def change
    create_table :variables do |t|
      t.string :name
      t.references :unit

      t.timestamps
    end
    add_index :variables, [:unit_id, :created_at]
  end
end
