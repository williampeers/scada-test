class Unit < ApplicationRecord
  has_many :variables,  dependent: :destroy 
  has_many :pierrors,  dependent: :destroy 
  has_one :user
  
  validates :name, presence: true
  validates :plc_ip, presence: true
  validates :plc_port, presence: true
  
  def to_param
    name
  end
  
  #validates_uniqueness_of :name, allow_blank: false, scope: :user
        
  def inputs
    self.variables.where(method: 'input')
  end
  
  def outputs
    self.variables.where(method: 'output')
  end
  
  def update
    #  HTTP.post("http://#{@user.name}-#{@unit.name}.green1.co.nz")
    #  HTTP.post("#{@unit.ip}:8000")
  end
  
  def config
    ret = {'plcreader' => Hash.new, 'scadaclient' => Hash.new }
    ret['plcreader'][:variables] = self.variables.select(:name, :register_address, :length, :bit_index)
    ret['plcreader'][:plc_ip] = self.plc_ip
    ret['plcreader'][:plc_port] = self.plc_port
    
    #ret['scadaclient'][:update_period] = self.update_period
    ret
  end
end
