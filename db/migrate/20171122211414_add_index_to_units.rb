class AddIndexToUnits < ActiveRecord::Migration[5.1]
  def change
    add_index :units, :identification
  end
end
