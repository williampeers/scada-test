class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  include SessionsHelper
  
  before_action :check_allowed_user
  
  def home
    if logged_in?
      if current_user
        redirect_to user_url(current_user)
      else
        redirect_to show_users_url
      end
    else
      redirect_to login_url
    end
  end
  
  private
    
    def check_allowed_user
      if not allowed_user?
        #store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
end
