class RenameDatapoint < ActiveRecord::Migration[5.1]
  def change
    rename_table :datapoint, :datapoints
  end
end
