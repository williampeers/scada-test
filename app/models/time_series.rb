class TimeSeries
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  
  field :name, type: String
  field :avg, type: Float, default: 0
  field :time, type: DateTime
  field :count, type: Integer, default: 0
  
  embeds_many :dataDays, counter_cache: true
  
  def new_value(value, time)
    day = Time.new(time.year, time.month, time.day)
    self.dataDays.find_or_create_by(day: day).new_value(value, time)
    count = self.count
    self.avg = (count * self.avg + value)/(count + 1)
    self.count = count + 1
    self.save
  end
  
  def days(length)
    output = {}
    self.dataDays.desc(:day).limit(length).each do |day|
      output[day.day] = day.avg unless day.day < Time.new(2017)
    end
    return output
  end
  
  def hours(length)
    output = {}
    count = 0
    self.dataDays.desc(:day).limit((length/24) + 1).each do |day|
      day.dataHours.desc(:hour).limit(length - count).each do |hour|
        output[hour.hour] = hour.avg unless hour.hour < Time.new(2017)
        count = count + 1
      end
    end
    return output
  end
  
  def mins(from)
    output = {}
    count = 0
    self.dataDays.desc(:day).where(:day.gte => from).each do |day|
      day.dataHours.desc(:hour).where(:hour.gte => from).each do |hour|
        hour.dataMinutes.desc(:min).where(:min.gte => from).each do |min|
          output[min.min] = min.avg unless hour.hour < Time.new(2017)
          count = count + 1
        end
      end
    end
    return output
  end
  
  def secs(length)
    output = {}
    count = 0
    self.dataDays.desc(:day).limit((length/24) + 1).each do |day|
      day.dataHours.desc(:hour).limit((length-count)/60 + 1).each do |hour|
        hour.dataMinutes.desc(:min).limit(length - count).each do |min|
          min.values.keys.each do |key|
            output[Time.at(key.to_i)] = min.values[key] unless Time.at(key.to_i) < Time.new(2017)
          end
        end
      end
    end
    return output
  end
end

class DataDay
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  
  field :avg, type: Float, default: 0
  field :day, type: DateTime
  field :count, type: Integer, default: 0

  def new_value(value, time)
    hour = Time.new(time.year, time.month, time.day, time.hour)
    self.dataHours.find_or_create_by(hour: hour).new_value(value, time)
    count = self.count
    self.avg = (count * self.avg + value)/(count + 1)
    self.count = count + 1
    self.save
  end
  
  embeds_many :dataHours
  embedded_in :time_series, counter_cache: true
end

class DataHour
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  
  field :avg, type: Float, default: 0
  field :hour, type: DateTime
  field :count, type: Integer, default: 0
  
  def new_value(value, time)
    min = Time.new(time.year, time.month, time.day, time.hour, time.min)
    self.dataMinutes.find_or_create_by(min: min).new_value(value, time)
    count = self.count
    self.avg = (count * self.avg + value)/(count + 1)
    self.count = count + 1
    self.save
  end
  
  embeds_many :dataMinutes
  embedded_in :dataDay, counter_cache: true
end

class DataMinute
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  
  field :avg, type: Float, default: 0
  field :min, type: DateTime
  field :count, type: Integer, default: 0
  field :values, :type => Hash, :default => {}
  
  def new_value(value, time)
    sec = Time.new(time.year, time.month, time.day, time.hour, time.min, time.sec)
    self.values[sec.to_i] = value
    count = self.count
    self.avg = (count * self.avg + value)/(count + 1)
    self.count = count + 1
    self.save
  end
  
  embedded_in :dataHour, counter_cache: true
end