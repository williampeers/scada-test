class AddDatapointReferenceToVariable < ActiveRecord::Migration[5.1]
  def change
    add_column :variables, :current_value, :decimal
    add_column :variables, :last_update, :datetime
  end
end
