class RenameTimeseries < ActiveRecord::Migration[5.1]
  def change
    rename_table :timeseries, :datapoint
  end
end
