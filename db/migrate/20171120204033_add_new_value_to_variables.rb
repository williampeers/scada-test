class AddNewValueToVariables < ActiveRecord::Migration[5.1]
  def change
    add_column :variables, :update_value, :boolean
    add_column :variables, :new_value, :decimal
  end
end
