class DataTotal
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  
  field :name, type: String
  field :avg, type: Float
  field :time, type: Datetime
  
  def new_value(value, time)
    self.dataDays.find_or_create_by(day: time.yday).new_value(value, time)
  end
  
  embeds_many :dataDays, counter_cache: true
end

class DataDay
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  
  field :avg, type: Float
  field :day, type: Integer
  
  def new_value(value, time)
    self.dataHours.find_or_create_by(hour: time.hour).new_value(value, time)
  end
  
  embeds_many :dataDays
  embedded_in :dataTotal, counter_cache: true
end

class DataHour
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  
  field :avg, type: Float
  field :hour, type: Integer
  
  def new_value(value, time)
    self.dataMinutes.find_or_create_by(min: time.min).new_value(value, time)
  end
  
  embeds_many :dataMinutes
  embedded_in :dataDay, counter_cache: true
end

class DataMinute
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  
  field :avg, type: Float
  field :min, type: Integer
  field :values, :type => Hash, :default => {}
  
  def new_value(value, time)
    self.values.find_or_create_by(sec: time.sec).value = value
  end
  
  embedded_in :dataHour, counter_cache: true
end