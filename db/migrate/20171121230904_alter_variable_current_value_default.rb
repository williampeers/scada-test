class AlterVariableCurrentValueDefault < ActiveRecord::Migration[5.1]
  def change
    change_column_default :variables, :current_value, 0
  end
end
