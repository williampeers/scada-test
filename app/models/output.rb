class Output < ApplicationRecord
  belongs_to :unit
  has_many :datapoints
  
  def update_current_value(value, time)
    self.current_value = value
    if ((time - self.last_update)).to_i >= 15
      self.datapoints.create(value: value, read_at: time)
      self.last_update = time
    end
    self.save
  end
  
  def to_param
    name
  end
end
