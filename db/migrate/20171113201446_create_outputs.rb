class CreateOutputs < ActiveRecord::Migration[5.1]
  def change
    create_table :outputs do |t|
      t.integer :register_address
      t.integer :bit_index
      t.integer :length
      t.string :units
      t.decimal :current_value
      t.datetime :last_update
      t.references :unit, foreign_key: true

      t.timestamps
    end
  end
end
