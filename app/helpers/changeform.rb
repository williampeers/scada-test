form_with(model: [@unit, variable], url: update_variable_url(@user, @unit, variable)) do |f| 
    f.label :new_value 
    f.text_field :new_value 
    hidden_field_tag :unit_id, unit.id 
    f.submit 
end 