module ApplicationHelper
  def strip(str)
    str.gsub('_', ' ')
  end
  
  def replace_whitespace(str)
    str.gsub(' ', '_')
  end
end
