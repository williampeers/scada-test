class InputsController < ApplicationController
  
  before_action :correct_user
  
  def create
    @unit = Unit.find_by(name: params[:unit_name])
    @input = @unit.inputs.build(input_params)
    if @input.save
      redirect_to @unit
    else
      redirect_to @unit
    end
  end
  
  def update
     @unit  = Unit.find_by(name: params[:unit_name])
     @input = @unit.inputs.find_by(name: params[:input_name])
     
     @input.update update_params
     @input.update(update_value: true)
       
      respond_to do |format|
        if @input.save
          format.html { redirect_to @user, notice: 'User was successfully created.' }
          format.js
          format.json { render json: @input, status: :updated, location: @input }
        else
          format.html { render action: "update" }
          format.json { render json: @input.errors, status: :unprocessable_entity }
        end
      end
  end
  
  def edit
    @unit  = current_user.units.find_by(name: params[:unit_name])
    @input = @unit.inputs.find_by(name: params[:input_name])
  end
  
  def patch
    @unit  = current_user.units.find_by(name: params[:unit_name])
    @input = @unit.inputs.find_by(name: params[:input_name])
    @input.update(input_params)
    redirect_to @unit
  end
  
  def new
    @unit  = Unit.find_by(name: params[:unit_name])
    @input = @unit.inputs.new
  end
  
  private
    def input_params
      params.require(:input).permit(:name, :register_address, :bit_index, :length, :units, :unit_id)
    end
    
    def update_params
      params.require(:input).permit(:new_value, :unit_id)
    end
    
    def correct_user
      unless allowed_user?
        redirect_to login_url
      end
    end
end
