class AddPasswordDigestToUnits < ActiveRecord::Migration[5.1]
  def change
    add_column :units, :password_digest, :string
  end
end
