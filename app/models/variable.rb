class Variable < ApplicationRecord
  belongs_to :unit
  has_many :pierrors, dependent: :destroy 
  
  validates :method, inclusion: { in: ['input', 'output'],
      message: "must be input or output" }
  
  validates :register_address, numericality: { 
              :only_integer => true, 
              :greater_than => 40000,
              :less_than => 50000, 
              :message => "requires integer between 40001 and 49999" }
  
  after_create :generateTimeSeries
  
  # retrieves mongodb object
  def data
    TimeSeries.find_by(name: self.dataName)
  end
  
  def received_new_value(value, time)
    self.working = true
    if value
      self.current_value = value
      if value == self.new_value
        self.update_value = false
      end
    
      self.data.new_value(value, Time.at(time))
    
      self.save
    end
  end
  
  def last_error
  end
  
  def to_param
    name
  end
  
  private
    def generateTimeSeries
      TimeSeries.create(name: self.dataName, time: Time.now)
      print("
      CREATED DATA
      ")
    end
end
