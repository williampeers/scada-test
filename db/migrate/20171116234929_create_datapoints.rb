class CreateDatapoints < ActiveRecord::Migration[5.1]
  def change
    create_table :datapoints do |t|
      t.datetime :read_at
      t.float :value
      t.references :output
    end
  end
end
