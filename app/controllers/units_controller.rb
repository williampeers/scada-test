class UnitsController < ApplicationController
  layout 'units'
  
  #before_action :load_unit, only: [:show, :edit, :patch]
  
  #before_action :correct_user, except: [:post_update]
  #before_action :correct_unit,   only: [:configuration, :post_update]

  def configuration
    @unit = current_user.units.find_by(name: params[:unit_name])
    render json: @unit.select(:name, :plc_ip, :plc_port).to_json
  end

  def pi_update
    @unit = Unit.find_by(identification: params[:identification])
    if @unit.psk != params[:psk]
      render json: {"error" => "Invalid passkey"}
      return
    end
    res = {}
    @unit.variables.each do |var|
      if params.key?(var.name)
        var.received_new_value(params[var.name], Time.at(params["Update_time"]))
      end
    end
    
    if params.key?('new_config') or @unit.new_config
      res['new_config'] = @unit.config
      @unit.new_config = false
      @unit.save
    end
    
    if params.key?('errors')
      if params['errors'].instance_of? Array
        params['errors'].each do |err|
          if var = @unit.variables.find_by(name: err['variable'])
            var.pierrors.build(details: err['details'], time: Time.at(params["Update_time"]))
            var.working = false
            var.save
          end
        end
      end
    end
    
    res["writes"] = @unit.variables.where(method: 'input', update_value: true).select(:name, :new_value)
    render :json => res
  end
  
  def show
    @user = current_user
    @unit = @user.units.find_by(name: params[:unit_name])
    #if request.xhr?
      #@unit.update
    #end
    
    respond_to do |format|
      format.html {render 'show'}
      format.js {render json: {"inputs" => @unit.inputs.select(:name, :current_value, :units, :working), "outputs" => @unit.outputs.select(:name, :current_value, :units, :working)}.to_json}
      format.xml  {render xml: {"inputs" => @unit.inputs.select(:name, :current_value, :units, :working), "outputs" => @unit.outputs.select(:name, :current_value, :units, :working)}}
    end
  end
  
  def new
    @unit = Unit.new
  end

  def create
    @unit = current_user.units.build(unit_params)
    @unit.save!
    #if @unit.save
    #  flash[:success] = "Unit Added"
      redirect_to current_user
    #else
    #  render 'new'
    #end
  end
  
  def delete
    @user = current_user
    @unit = @user.units.find_by(name: params[:unit_name])
  end
  
  def destroy
    @user = current_user
    @unit = @user.units.find_by(name: params[:unit_name])
    @unit.delete
    @unit.save
    redirect_to @user
  end
  
  def edit
    @user = current_user
    @unit = @user.units.find_by(name: params[:unit_name])
  end
  
  def patch
    @user = current_user
    @unit = @user.units.find_by(name: params[:unit_name])
    @unit.update(unit_params)
    redirect_to @unit
  end
  
  private
    def unit_params
      params[:unit][:name].gsub!(' ', '_')
      params.require(:unit).permit(:name, :description, :plc_ip, :plc_port, :ip, :psk, :user, :identification)
    end

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        redirect_to login_url
      end
    end

    # Confirms the correct user.
    def correct_user
      unless allowed_user?
        redirect_to login_url
      end
    end
end