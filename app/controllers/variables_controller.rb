class VariablesController < ApplicationController
  layout 'units'
  
  def show
    @unit = current_user.units.find_by(name: params[:unit_name])
    @variable = @unit.variables.find_by(name: params[:variable_name])
    if @variable.working == false
      @error = @variable.pierrors.last
    end
  end
  
  def new
    @unit  = Unit.find_by(name: params[:unit_name])
    @variable = @unit.variables.new
  end
  
  def chart
    @unit = current_user.units.find_by(name: params[:unit_name])
    @variable = @unit.variables.find_by(name: params[:variable_name])
    period = params[:period]
    from = Time.new(2017, 11, 27, 23, 30) #params[:from]
    
    if period == "days"
      data = @variable.data.days(from) 
    elsif period == "hours"
      data = @variable.data.hours(from) 
    elsif period == "mins"
      data = @variable.data.mins(from) 
    elsif period == "secs"
      data = @variable.data.secs(from) 
    end

    respond_to do |format|
      format.html { render html: draw(data, period, from), layout: false }
      format.json { render json: data }
    end
    
  end
  
  def create
    @unit = Unit.find_by(name: params[:unit_name])
    @variable = @unit.variables.build(variable_params)
    @variable.dataName = params[:user_name] + "-" + params[:unit_name] + "-" + @variable.name
    if @variable.save
      flash[:success] = "Variable Successfully created"
      @unit.new_config = true
      @unit.save
      redirect_to unit_url(current_user, @unit)
    else
      render 'new', :method => params[:method]
    end
  end
  
  def edit
    @unit  = current_user.units.find_by(name: params[:unit_name])
    @variable = @unit.variables.find_by(name: params[:variable_name])
  end
  
  def patch
    @unit  = current_user.units.find_by(name: params[:unit_name])
    @variable = @unit.variables.find_by(name: params[:variable_name])
    if @variable.update(variable_params)
      @unit.new_config = true
      @unit.save
    end
    redirect_to variable_url(current_user, @unit, @variable)
  end
  
  def delete
    @user = current_user
    @unit = @user.units.find_by(name: params[:unit_name])
    @variable = @unit.variables.find_by(name: params[:variable_name])
  end
  
  def destroy
    @user = current_user
    @unit = @user.units.find_by(name: params[:unit_name])
    @variable = @unit.variables.find_by(name: params[:variable_name])
    unless params[:given_user_name] == "imsure" or (@user.name == params[:given_user_name] and @unit.name == params[:given_unit_name] and @variable.name == params[:given_variable_name])
      flash[:danger] = "Wrong variables details"
      redirect_to delete_variable_url(@user, @unit, @variable)
    else
      @variable.delete
      @variable.save
      @unit.new_config = true
      @unit.save
      redirect_to unit_url(@user, @unit)
    end
  end
  
  def update
      @unit  = Unit.find_by(name: params[:unit_name])
      @variable = @unit.variables.find_by(name: params[:variable_name])
     
      @variable.update(update_value: true, new_value: params[:variable][:new_value])
       
      respond_to do |format|
        if @variable.save
          print("
          SAVED
          ")
          format.html { redirect_to @user}
          format.js
          format.json { render json: @variable, status: :updated, location: @variable }
        else
          format.html { render action: "update" }
          format.json { render json: @variable.errors, status: :unprocessable_entity }
        end
      end
  end
  
  private
    def variable_params
      params[:variable][:name].gsub!(' ', '_')
      params.require(:variable).permit(:name, :register_address, :bit_index, :length, :units, :unit_id, :method)
    end
    
    def update_params
      params.require(:new_value)
    end
    
    def draw(data, period, from)
    
      if @variable.units != "boolean"
        units = " (#{@variable.units})"
      else
        units = nil
      end
  
      line_chart chart_url({period: period, from: from}), refresh: 300, library: {
        scales: {
          yAxes: [
            {
              display: true,
              reverse: true,
              scaleLabel:{
                display: true,
                labelString: "#{@variable.name.gsub('_', ' ')}#{units}"
              }
            }
          ],
          xAxes: [
            time: {
              max: Time.now,
              min: from
            }
          ]
        }
      }
    end
end
