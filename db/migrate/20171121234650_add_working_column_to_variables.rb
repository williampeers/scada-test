class AddWorkingColumnToVariables < ActiveRecord::Migration[5.1]
  def change
    add_column :units, :working, :boolean, default: true
    add_column :variables, :working, :boolean, default: true
  end
end
