class AddMoreColumnsToUnits < ActiveRecord::Migration[5.1]
  def change
    add_column :units, :long_update_period, :integer, default: 300
    rename_column :units, :update_period, :short_update_period
    change_column_default :units, :short_update_period, 1
  end
end
